class CreateLeaderboards < ActiveRecord::Migration
  def change
    create_table :leaderboards do |t|
      t.integer :position
      t.string :name
      t.integer :score
      t.string :email

      t.timestamps null: false
    end
  end
end
