json.array!(@leaderboards) do |leaderboard|
  json.extract! leaderboard, :id, :position, :name, :score, :email
  json.url leaderboard_url(leaderboard, format: :json)
end
